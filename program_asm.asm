extern printf

SECTION .data
    fmt_result_before:         db "Result before setup 4 bits to 1 = %d", 10, 0
    fmt_result_after:           db "Result after setup 4 bits to 1 = %d", 10, 0
    number_elements:      dd  10  ; 5 elements, 10 byte
    array_word            dw  10, 20, 30, 40, 50

SECTION .bss
    result_before: resb 4
    result_after: resb 4

SECTION .text
GLOBAL	main
main:
    mov ebx, array_word
    mov ecx, -2
    loop_for_start:
        mov eax, 0
        add ecx, 2
        cmp ecx, [number_elements]      ; condition for loop
        je loop_for_end

        mov ax, [array_word+ecx]        ; get current element
        add [result_before], eax
        or eax, 1111b                   ; set up 1111 in all bits array
        add [result_after], eax
        jmp loop_for_start

    loop_for_end:

    mov	rdi, fmt_result_before
    mov	rsi, [result_before]
    mov	rax, 0
    call	printf

    mov	rdi, fmt_result_after         ; first argument for printf
    mov	rsi, [result_after]           ; second argument (printf)
    mov	rax, 0
    call	printf

    mov     rax, 60         ; "exit" function number
    xor     rdi, rdi        ; error code (0)
    syscall                 ; terminate the program